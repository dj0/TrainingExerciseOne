package ex01;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class Colours {
	public static void printColours() {
		System.out.println("Please choose a colour or write exit to terminate: ");
		System.out.print("Black ");
		System.out.print("White ");
		System.out.print("Red ");
		System.out.print("LightGreen ");
		System.out.print("Yellow ");
		System.out.print("Grey ");
		System.out.print("Orange ");
		System.out.print("Violet" + "\r\n");
		System.out.print("Purple ");
		System.out.print("Cyan ");
		System.out.print("Magenta ");
		System.out.print("DarkGreen ");
		System.out.print("Aqua ");
		System.out.print("Ivory ");
		System.out.print("SkyBlue" + "\r\n");
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		HashMap<String, String> colours = new HashMap<>();
		colours.put("Black", "#000000");
		colours.put("White", "#FFFFFF");
		colours.put("Red", "#FF0000");
		colours.put("LightGreen", "#90EE90");
		colours.put("Yellow", "#FFd700");
		colours.put("Grey", "#808080");
		colours.put("Orange", "#FFA500");
		colours.put("Violet", "##EE82EE");
		colours.put("Purple", "#800080");
		colours.put("Cyan", "#00FFFF");
		colours.put("Magenta", "#FF00FF");
		colours.put("Green", "#008000");
		colours.put("Aqua", "#00FFFF");
		colours.put("Ivory", "#FFFFF0");
		colours.put("SkyBlue", "#87CEEB");
		
		printColours();
		System.out.println();
		
		System.out.println("Please choose a colour or write exit to terminate: ");
		
		String input = null;
		while((input = scanner.nextLine()) != null) {
			if(input.toLowerCase().equals("exit")) {
				return;
			}
			
			for (Entry<String, String> entry : colours.entrySet()) {
				if (entry.getKey().toLowerCase().equals(input.toLowerCase())) {
					System.out.println("You chose: " + entry.getKey() + " " + entry.getValue());
				}
			}
			
			printColours();
			
		}
		
		scanner.close();
	}
}
