package ex03;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class TimeTest{
	
	static HashMap<Integer, Integer> hMapOne = new HashMap<>(10000);
	static HashMap<Integer, Integer> hMapTwo = new HashMap<>(100000);
	static HashMap<Integer, Integer> hMapThree = new HashMap<>(1000000);
	static ConcurrentHashMap<Integer, Integer> cMapOne = new ConcurrentHashMap<>(10000, 0.75F, 3);
	static ConcurrentHashMap<Integer, Integer> cMapTwo = new ConcurrentHashMap<>(100000, 0.75F, 3);
	static ConcurrentHashMap<Integer, Integer> cMapThree = new ConcurrentHashMap<>(1000000, 0.75F, 3);
	
	public static void main(String[] args) {
		smallMapH(hMapOne);
		smallMapC(cMapOne);
		mediumMapH(hMapTwo);
		mediumMapC(cMapTwo);
		largeMapH(hMapThree);
		largeMapC(cMapThree);
	}
	
	public static void fillMap(ConcurrentHashMap<Integer, Integer> map , int size) {
		Random rng = new Random();
		for (int i = 0; i < size; i++) {
			int value = rng.nextInt(size*2);
			map.putIfAbsent(i, value);
		}
	}

	public static void smallMapH(HashMap<Integer, Integer> hMap) {
		Random rng = new Random();

		long startTimeHash = System.nanoTime();
		for (int i = 0; i < 10000; i++) {
			int smallValue = rng.nextInt(20000);
			hMap.put(i, smallValue);
		}
		long estimatedTimeHash = System.nanoTime() - startTimeHash;

		System.out.println("The time to map the small HashMap: " + estimatedTimeHash + " nanoseconds.");
	}

	public static void smallMapC(ConcurrentHashMap<Integer, Integer> cMap) {
		long startTimeConc = System.nanoTime();

		Runnable fillS = () -> fillMap(cMapOne, 10000);
		
		new Thread(fillS).start();
		new Thread(fillS).start();
		new Thread(fillS).start();
		
		while (cMap.size() < 10000) {
			;
		}


		long estimatedTimeConc = System.nanoTime() - startTimeConc;

		int counter = 0;
		long startTimeMatches = System.nanoTime();
		for (Entry<Integer, Integer> entry : cMap.entrySet()) {
			int key = entry.getKey();
			for (Entry<Integer, Integer> entry2 : cMap.entrySet()) {
				if (key == entry2.getValue()) {
					counter++;
				}
			}
		}

		long estimatedTimeMatches = System.nanoTime() - startTimeMatches;
		System.out.println("The time to map the small ConcurrentHashMap: " + estimatedTimeConc + " nanoseconds.");
		System.out.println("The times keys equal values = " + counter);
		System.out.println("The time spent to find all the matches is: " + estimatedTimeMatches + " nanoseconds.");
	}

	public static void mediumMapH(HashMap<Integer, Integer> hMap) {
		Random rng = new Random();

		long startTimeHash = System.nanoTime();
		for (int i = 0; i < 100000; i++) {
			int mediumValue = rng.nextInt(200000);
			hMap.put(i, mediumValue);
		}
		long estimatedTimeHash = System.nanoTime() - startTimeHash;

		System.out.println("The time to map the medium HashMap: " + estimatedTimeHash + " nanoseconds.");
	}

	public static void mediumMapC(ConcurrentHashMap<Integer, Integer> cMap) {
		long startTimeConc = System.nanoTime();

		Runnable fillM = () -> fillMap(cMapTwo, 100000);
		
		new Thread(fillM).start();
		new Thread(fillM).start();
		new Thread(fillM).start();
		
		while (cMap.size() < 100000) {
			;
		}

		long estimatedTimeConc = System.nanoTime() - startTimeConc;

		System.out.println("The time to map the medium ConcurrentHashMap: " + estimatedTimeConc + " nanoseconds.");
	}

	public static void largeMapH(HashMap<Integer, Integer> hMap) {
		Random rng = new Random();

		long startTimeHash = System.nanoTime();
		for (int i = 0; i < 1000000; i++) {
			int largeValue = rng.nextInt(2000000);
			hMap.put(i, largeValue);
		}
		long estimatedTimeHash = System.nanoTime() - startTimeHash;

		System.out.println("The time to map the large HashMap: " + estimatedTimeHash + " nanosecond.s");
	}

	public static void largeMapC(ConcurrentHashMap<Integer, Integer> cMap) {
		long startTimeConc = System.nanoTime();

		Runnable fillL = () -> fillMap(cMapThree, 1000000);
		
		new Thread(fillL).start();
		new Thread(fillL).start();
		new Thread(fillL).start();
		
		while (cMap.size() < 1000000) {
			;
		}

		long estimatedTimeConc = System.nanoTime() - startTimeConc;

		System.out.println("The time to map the large ConcurrentHashMap: " + estimatedTimeConc + " nanoseconds.");
	}
}
