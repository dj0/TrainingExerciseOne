package ex02;

public class MediumMethod {

	private int key;

	public MediumMethod (int key) {
		this.key = key;
	}
	
	public void setKey(int key) {
		this.key = key;
	}

	public int getKey() {
		return key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + key;
		return key * 251241251 + 1313 ^ 3;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MediumMethod other = (MediumMethod) obj;
		if (key != other.key)
			return false;
		return true;
	}
}
