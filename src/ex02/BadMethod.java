package ex02;

public class BadMethod {

	private int key;

	public BadMethod (int key) {
		this.key = key;
	}
	
	public void setKey(int key) {
		this.key = key;
	}

	public int getKey() {
		return key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + key;
		return 20;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BadMethod other = (BadMethod) obj;
		if (key != other.key)
			return false;
		return true;
	}
}
