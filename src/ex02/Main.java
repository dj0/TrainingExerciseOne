package ex02;

import java.util.HashMap;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		int key = 20;
		Random rng = new Random();

		GoodMethod good = new GoodMethod(key);
		MediumMethod medium = new MediumMethod(key);
		BadMethod bad = new BadMethod(key);

		HashMap<Integer, Integer> goodMap = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> mediumMap = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> badMap = new HashMap<Integer, Integer>();

		long startTimeHashB = System.nanoTime();
		int badRng = rng.nextInt(10000);
		badMap.put(bad.getKey(), badRng);
		long estimatedTimeHashB = System.nanoTime() - startTimeHashB;

		long startTimeHashM = System.nanoTime();
		int mediumRng = rng.nextInt(10000);
		mediumMap.put(medium.getKey(), mediumRng);
		long estimatedTimeHashM = System.nanoTime() - startTimeHashM;

		long startTimeHashG = System.nanoTime();
		int goodRng = rng.nextInt(10000);
		goodMap.put(good.getKey(), goodRng);
		long estimatedTimeHashG = System.nanoTime() - startTimeHashG;

		System.out.println("The time needed to map the bad HashMap was: " + estimatedTimeHashB + " nanoseconds.");
		System.out.println("The time needed to map the medium HashMap was: " + estimatedTimeHashM + " nanoseconds.");
		System.out.println("The time needed to map the good HashMap was: " + estimatedTimeHashG + " nanoseconds.");
	}
}
