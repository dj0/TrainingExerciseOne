package ex02;

public class GoodMethod {

	private int key;
	
	public GoodMethod (int key) {
		this.key = key;
	}
	
	public void setKey(int key) {
		this.key = key;
	}

	public int getKey() {
		return key;
	}
	
	@Override
	public int hashCode() {
		
		return key;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GoodMethod other = (GoodMethod) obj;
		if (key != other.key)
			return false;
		return true;
	}
}
